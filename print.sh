#!/bin/bash
SCALE="1.1"

print() {
  opts="-o Option1=False -o PageSize=Letter -o PageRegion=Letter ";
  opts="${opts} -o InputSlot=Default -o Duplex=None";
  lpr -P Xerox_Phaser_6700DN $opts "$1"
}

listfiles() {
  if [[ -n "$1" ]]; then
    ls cards/*.pdf | grep -i "$1";
  else
    ls cards/*.pdf;
  fi
}

listfiles "$1" | while read filename; do
  filename=$(basename "$filename");
  rm "/tmp/$filename" &> /dev/null;
  pdfjam --quiet --nup 1x2 --scale $SCALE "cards/$filename" -o "/tmp/$filename" &&
  echo "printing $filename" &&
  print "/tmp/$filename";
done

# vim: et sw=2 ts=2
