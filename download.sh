#!/bin/bash
FOLDER_ID="1-mfhBuOtwoulj1mNTCfhaLPxKkjgd6JV"
cd cards;
query="'$FOLDER_ID' in parents";
if [[ -n "$1" ]]; then
    query="$query and name contains '$1'";
fi
gdrive list --no-header --query "$query" | grep -v halfsheet | cut -d ' ' -f 1 | while read id; do
    gdrive export -f --mime application/pdf "$id";
done
cd ..
